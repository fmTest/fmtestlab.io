import * as css from './index.css'
import fetch from 'node-fetch';
import adapter from 'webrtc-adapter';
import {DiffCamEngine} from '../../libs/diff-cam-engine'
import * as faceapi from 'face-api.js';

export default class App {
  constructor (elem) {
    navigator.getMedia = ( navigator.getUserMedia ||
      navigator.webkitGetUserMedia ||
      navigator.mozGetUserMedia ||
      navigator.msGetUserMedia);
    if (!elem) return
    this.elem = elem
  }

  render() {
    this.elem.innerHTML = `
      <section data-component="app">
        <h1>Place your document here!! ↓</h1>
        <canvas id="canvas">
        </canvas>
        <video id="webcam-source" style="display: none"></video>
        <p id="guideText">Loading Models</p>
        <div id="pop-up" class="pop-up hide">
          <div class="wrapper">
            <h2>Document Captured</h2>
            <img src="" id="result"/>
            <button id="dismissBtn" >Close</button>
          </div>
        </div>
      </section>
      `
  }

  startListener() {
    this.obj = {
      video: document.getElementById('webcam-source'),
      canvas: document.getElementById('canvas'),
      result: document.getElementById('result'),
      popUp: document.getElementById('pop-up'),
      guideText: document.getElementById('guideText')
    }
    this.opt = {
      refreshRate: 100,
      width: 1280,
      height: 720,
      bufferSize: 5,
      minScore: 0.95,
    };
    this.state = {
      detectionsBuffer: new Array(), //Buffer
      detecting : false,
      camEngine : DiffCamEngine
    };
    this.obj.canvas.width = this.obj.canvas.scrollWidth;
    this.obj.canvas.height = this.obj.canvas.scrollHeight;
    this.state.camEngine.init({
      video: this.obj.video,
      captureWidth: this.opt.width,
      captureHeight: this.opt.height,
      captureIntervalTime: this.opt.refreshRate,
      initSuccessCallback: this.drawImage.bind(this),
      captureCallback: this.scoreImageMovement.bind(this),
    });

    document.getElementById("dismissBtn").addEventListener('click', () => {console.log("close with event");this.closePopup()})
  }
  
  async loadModels() {
    const MODEL_URL = '/models'
    await faceapi.loadSsdMobilenetv1Model(MODEL_URL)
    await faceapi.loadFaceLandmarkModel(MODEL_URL)
    await faceapi.loadFaceRecognitionModel(MODEL_URL)
    return true
  }

  drawImage(stream) {
    this.state.camEngine.start.bind(this);
    if(!this.state.popUp) {
      this.stream = stream;
      this.obj.video.srcObject = stream;
      this.obj.video.play();
      this.interval = setInterval(this.canvasDraw.bind(this), this.opt.refreshRate);
    } else {
      this.state.camEngine.stop(this.stream).bind(this);
    }
  }

  calculateSize(srcSize, dstSize) {
    var srcRatio = srcSize.width / srcSize.height;
    var dstRatio = dstSize.width / dstSize.height;
    if (dstRatio > srcRatio) {
      return {
        width:  dstSize.height * srcRatio,
        height: dstSize.height
      }
    } else {
      return {
        width:  dstSize.width,
        height: dstSize.width / srcRatio
      }
    }
  }

  canvasDraw() {
    this.context = this.obj.canvas.getContext('2d');
    var videoSize = { width: this.obj.video.videoWidth, height: this.obj.video.videoHeight };
    var canvasSize = { width: this.obj.canvas.width, height: this.obj.canvas.height };
    var renderSize = this.calculateSize(videoSize, canvasSize);
    var xOffset = (canvasSize.width - renderSize.width) / 2;
    this.context.drawImage(this.obj.video, xOffset, 0, renderSize.width, renderSize.height);

    if(!this.state.detecting) {
      this.state.camEngine.capture()
    }
  }

  async faceDetection() {
    const snapshot = this.obj.canvas.toDataURL("image/png");
    this.state.detecting = true;
    await faceapi.detectSingleFace(this.obj.canvas).withFaceLandmarks().then(result => {
      console.log("Image recieved");
      console.log(result);
      if(result){
        this.state.detectionsBuffer.push({result: result, snap: snapshot});
        if(this.state.detectionsBuffer.length >= this.opt.bufferSize) {
          this.analizeData();
        }
        faceapi.draw.drawFaceLandmarks(this.obj.canvas, result);
      } else {
        this.setGuideMessage("No face detected")
        //Timeout?
      }
    }, rejected =>{
      console.log("error");
      console.log(rejected)
    }).finally(()=>{this.state.detecting = false})
  }

  async analizeData() {
    this.state.popUp = true;
    var min = null;
    var currScore = 0;
    for(const d of this.state.detectionsBuffer){
      var detection = d.result.detection;
      if(detection.score > this.opt.minScore && detection.score > currScore) {
        currScore = detection.score;
        min = d
      }
    }
    if(min !== null) {
      this.obj.result.src = min.snap;
      this.showPopup()
    } else {
      this.closePopup()
    }
  }

  showPopup() {
    this.state.popUp = true;
    this.obj.popUp.className = "pop-up"
  }

  closePopup() {
    this.state.detectionsBuffer = new Array()
    this.state.popUp = false;
    this.obj.popUp.className = "pop-up hide"
  }

  scoreImageMovement(object) {
    if(object !== undefined) {
      if(!object.hasMotion ) {
        if(!this.state.popUp) {
          this.faceDetection()
        }
        else {
          this.setGuideMessage("Analizing detections")
        }
      } else {
        this.setGuideMessage('We detected movement')
      }
    }
  }

  setGuideMessage(text) {
    if(text !== undefined) {
      this.obj.guideText.innerText = text;
    }
  }
}